package routes

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"smartHomeNode/v1/base"
	"smartHomeNode/v1/config"
	"smartHomeNode/v1/http/Controllers"
)

var (
	Login  Controllers.LoginController
	Switch Controllers.SwitchController
	Time   Controllers.TimeController
	Button Controllers.ButtonController
	Main   Controllers.MainController
)

type Routes struct {
	base.Base
}

/**
 * 路由
 */
func (stx *Routes) Routes() {
	fmt.Println("Starting http server ...")
	gin.SetMode(config.GINMODE)
	router := gin.Default()
	// 前端页面
	router.LoadHTMLGlob("./index.html")
	router.Static("static/css", "static/css").Static("static/js", "static/js").
		Static("static/fonts", "static/fonts").Static("static/img", "static/img")
	router.GET("./", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "Main website",
		})
	})

	// 登录方法
	admin := router.Group("/api/admin")
	{
		admin.POST("/login", Login.Login)
		admin.POST("/logout", Login.Logout)
		admin.GET("/time", Login.Time)
	}

	// 控制方法
	api := router.Group("/api")
	{
		/*--------- 开关控制 ---------*/
		api.GET("/switch/index", Switch.Index)
		api.POST("/switch/update", Switch.Update)
		api.POST("/switch/control", Switch.Control)

		/*--------- 定时控制 ---------*/
		api.GET("/time/index", Time.Index)
		api.POST("/time/update", Time.Update)
		api.POST("/time/control", Time.Control)

		/*--------- 按钮控制 ---------*/
		api.GET("/button/index", Button.Index)
		api.POST("/button/control", Button.Control)
		api.POST("/button/delay", Button.Delay)
		api.GET("/button/control/:name/:control", Button.UrlControl)
		api.GET("/button/delay/:name/:delay", Button.UrlDelayControl)

		/*--------- 首页控制 ---------*/
		api.GET("/main/indexTemp", Main.IndexTemp)
		api.GET("/main/indexHum", Main.IndexHum)
		api.GET("/main/chartTemp", Main.ChartTemp)
		api.GET("/main/chartHum", Main.ChartHum)
		api.POST("/main/update", Main.Update)

	}

	router.Run(stx.Args())
}
